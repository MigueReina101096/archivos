module.exports.crearBuscarUsuario = (arregloUsuarios, usuarioBuscarCrear, cb) => {
    buscarUsuario(arregloUsuarios, usuarioBuscarCrear, (resultadoBusqueda) => {
        if (resultadoBusqueda.usuarioEncontrado) {
            cb({
                mensaje: 'usuario encontrado',
                usuario: resultadoBusqueda.usuarioEncontrado
            })
        } else {
            crearUsuario(arregloUsuarios, usuarioBuscarCrear, (resultadoUsuarioCreado) => {
                cb({
                    mensaje: 'usuario no encontrado, usuario creado',
                    arregloUsuarios: resultadoUsuarioCreado.arregloUsuarios
                })
            })
        }
    })
}