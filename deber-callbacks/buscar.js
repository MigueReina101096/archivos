module.exports.buscarUsuario = (arregloUsuarios, usuarioBuscar, cb) => {
    const usuarioEncontrado = arregloUsuarios.find((usuario) => {
        return usuarioBuscar === usuario;
    });
    if (usuarioEncontrado) {
        cb({
            mensaje: 'usuario encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    } else {
        cb({
            mensaje: 'usuario no encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    }
}

//promesas
module.exports.buscarUsuarioPromise = (arregloUsuarios, usuarioBuscar) => {
    const usuarioEncontrado = arregloUsuarios.find((usuario) => {
        return usuarioBuscar === usuario;
    });
    return new Promise((resolve,reject) => {
        if (usuarioEncontrado) {
            resolve({
                mensaje: 'usuario encontrado',
                usuarioEncontrado: usuarioEncontrado
            })
        } else {
            reject({
                mensaje: 'usuario no encontrado',
                usuarioEncontrado: usuarioEncontrado
            })
        }
    })
}