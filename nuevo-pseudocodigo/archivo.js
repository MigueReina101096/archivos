/* //crear
module.exports.crearUsuario = (arregloUsuarios, usuarioCrear, cb) => {
    arregloUsuarios.push(usuarioCrear)
    cb({
        mensaje: 'se creo',
        arregloUsuarios: arregloUsuarios
    })
};

//buscar
module.exports.buscarUsuario = (arregloUsuarios, usuarioBuscar, cb) => {
    const usuarioEncontrado = arregloUsuarios.find((usuario) => {
        return usuarioBuscar === usuario;
    });
    if (usuarioEncontrado) {
        cb({
            mensaje: 'usuario encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    } else {
        cb({
            mensaje: 'usuario no encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    }
}

//buscar y crear
module.exports.crearBuscarUsuario = (arregloUsuarios, usuarioBuscarCrear, cb) => {
    buscarUsuario(arregloUsuarios, usuarioBuscarCrear, (resultadoBusqueda) => {
        if (resultadoBusqueda.usuarioEncontrado) {
            cb({
                mensaje: 'usuario encontrado',
                usuario: resultadoBusqueda.usuarioEncontrado
            })
        } else {
            crearUsuario(arregloUsuarios, usuarioBuscarCrear, (resultadoUsuarioCreado) => {
                cb({
                    mensaje: 'usuario no encontrado, usuario creado',
                    arregloUsuarios: resultadoUsuarioCreado.arregloUsuarios
                })
            })
        }
    })
} */

//buscar para eliminar
module.exports.buscarUsuarioparaEliminar = (arregloUsuarios, usuarioBuscar, cb) => {
    const usuarioEncontrado = arregloUsuarios.find((usuario) => {
        const indiceUsuario = arregloUsuarios.indexof(usuarioEncontrado)
        if(usuarioBuscar === usuario){
            return indiceUsuario
        }else{
            return 'No existe dicho usuario'
        }
    });
    /*if (usuarioEncontrado) {
        cb({
            mensaje: 'usuario encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    } else {
        cb({
            mensaje: 'usuario no encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    }*/ 
}

//Eliminar 
module.exports.elminiarUsuario = (arregloUsuarios, usuarioEliminar,cb)=>{
    buscarUsuarioparaEliminar(arregloUsuarios, usuarioEliminar)
} 