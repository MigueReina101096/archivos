const sumar = require('./suma.js');
const restar = require('./resta.js');
const multiplicar = require('./multiplicacion.js');
const dividir = require('./division.js');


module.exports = {
    sumar,
    restar,
    multiplicar,
    dividir
}