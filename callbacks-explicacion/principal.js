const saludo = require('./saludar.js');
const miFuncionCallback = (resultado) => {
    console.log(resultado)
}
saludo(miFuncionCallback)

const deber = require('./deberes.js')
const infoUsuario = { nombre: 'Miguel', cumplioDeber: true }
const funcionValidacion = (resultado) => {
    console.log(resultado)
}

deber.verificarCallbacks(infoUsuario, funcionValidacion)
deber.verificarPromesas(infoUsuario)
    .then(respuestaThen => {
        console.log('Then->Resolve', respuestaThen)
        return deber.verificarPromesas({ nombre: 'Oscar', cumplioDeber: false })
    })
    .then(respuestaThenReturn =>{
        console.log('Then Return', respuestaThenReturn)
    })
    .catch(respuestaCatch => {
        console.log('Catch->Reject', respuestaCatch)
    })