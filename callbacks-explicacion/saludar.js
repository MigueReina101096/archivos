module.exports = (cb) => { //Esta es una funcion que se va a exportar
    cb({
        saludo: 'Hola, como estas.'
    })
};

//Un callback es una funcion que se ejecuta 
//dentro de otra funcion para una ejecucion 
//de manera sincronica

//En este caso mi JSON es un parametro 
//y puedo enviar n parametros