module.exports.verificarCallbacks = (usuario, cb) => {
    const cumplioDeber = usuario.cumplioDeber
    if (cumplioDeber) {
        cb({
            mensaje: usuario.nombre + ' Cumplio el deber'
        })
    } else {
        cb({
            mensaje: `${usuario.nombre} No cumplio el deber`
        })
    }
}
module.exports.verificarPromesas = (usuario) => {
    return new Promise((resolve, reject) => {
        const cumplioDeber = usuario.cumplioDeber
        if (cumplioDeber) {
            resolve({
                mensaje: usuario.nombre + ' Cumplio el deber'
            })
        } else {
            reject({
                mensaje: `${usuario.nombre} No cumplio el deber`
            })
        }
    })
}