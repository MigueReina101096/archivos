const fs = require('fs');
module.exports = (archivo,mensaje,cb)=>{
fs.writeFile(archivo, mensaje,(error) => {
    if(error){
        cb({
            ERROR:error,
            Info: 'Crea un nuevo archivo'
        })
    }else{
        cb({
            ERROR:error,
            Info: 'Solamente lee el archivo'
        })
    }
});
};