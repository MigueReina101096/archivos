const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

const ejemplo$ = of(1, 2, 3, 4, '7', 6, 7, 8, 9)
/* ejemplo$.subscribe(respuesta=>{
    console.log(respuesta)
}) */
const respuesta = (respuesta) => {
    console.log(respuesta)
}
const error = (error) => {
    console.log(error)
    //throw ({error: 'error'})
}
const cuanFinaliza = () => {
    console.log('Culmina')
}

function sumarUno(numero) {
    return Number(numero) + 1
}
function multiplicarDos(numero) {
    return numero * 2
}
function filtrarPares(numero) {
    return numero % 2 == 0
}

function promesa(numero) {
    const callBackPromesa = (resolve, reject) => {
        if (numero % 2 === 0) {
            reject(numero)
        } else {
            // console.log('deberia ejecutarce')
            resolve(numero)
        }
    }
    return new Promise(callBackPromesa)
}

/* function callbackEjemplo(miCb) {
    miCb('termino')
    miCb('termino otra vez')
    miCb('ya ahora si quiero terminar')
}

callbackEjemplo((miR) => {
    console.log(miR)
}) */

/* promesa(4)
    .then(respuesta => {
        console.log('resolve', respuesta)
    })
    .catch(respuesta => {
        console.log('reject', respuesta)
    }) */

const ejecutarPromesa = (numero) => {
    //promesa(numero)
    const observablePromesa$ = from(promesa(numero))
    return observablePromesa$
}

ejemplo$
    .pipe(
        //filter(filtrarPares),
        map(sumarUno),
        /*map(multiplicarDos),
        distinct(),
        filter(filtrarPares), */
        mergeMap(ejecutarPromesa)//devuelve un observable
    )
    .subscribe(respuesta, error, cuanFinaliza)